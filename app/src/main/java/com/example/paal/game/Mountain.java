package com.example.paal.game;

import android.graphics.Rect;

/**
 * Created by Pål on 01.03.2017.
 */

public class Mountain {

    private Rect imgRect;

    private double x;
    private double y;

    private double speedDiff;

    private int width;
    private int height;

    public Mountain(Rect imgRect, int x, int y){
        this.imgRect = imgRect;

        this.width = imgRect.width();
        this.height = imgRect.height();

        this.x = x;
        this.y = y;

        this.speedDiff = 0.2 + (Math.random()/4);
    }

    public double getY() {
        return y;
    }

    public double getX() {
        return x;
    }

    public double getSpeedDiff() {
        return speedDiff;
    }

    public Rect getImgRect() {
        return imgRect;
    }

    public void setPos(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void changePos(double dx, double dy){
        this.x += dx;
        this.y += dy;
    }

    public Rect getPlaceRect(){
        return new Rect((int)(x), (int)(y - height*(speedDiff-0.2)), (int)(x + width + width*(speedDiff-0.2)), (int)(y+height));
    }
}
