package com.example.paal.game;

/**
 * Created by Pål on 20.02.2017.
 */

public class Ammo {
    private int x;
    private int y;

    private double dx;
    private double dy = 0;

    private double gravity = 0.06;

    public Ammo(int x, int y, double dx){
        this.x = x;
        this.y = y;

        this.dx = dx;
        this.dy = 0;
    }

    public void move(boolean bxChanging){
        dy += gravity;

        if(bxChanging){
            x += dx/2;
        } else {
            x += dx;
        }
        y += dy;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }
}
