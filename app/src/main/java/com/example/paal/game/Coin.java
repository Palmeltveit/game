package com.example.paal.game;

import android.graphics.Rect;

/**
 * Created by Pål on 05.03.2017.
 */

public class Coin {

    private int x;
    private int y;
    private Tile dependencyTile;

    private boolean onTopTile = false;

    private Player player;
    private boolean lockedToPlayer = false;

    public Coin(Tile dependencyTile, int x, int y, boolean onTopTile){
        this.dependencyTile = dependencyTile;

        this.x = x;
        this.y = y;

        this.onTopTile = onTopTile;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void lockToPlayer(Player player){
        this.player = player;
        lockedToPlayer = true;
    }

    public boolean isOnTopTile() {
        return onTopTile;
    }

    public boolean moveToPlayer(){


        if(onTopTile) {
            return (y + dependencyTile.getTopTile().getPlaceRect().top - dependencyTile.getPlaceRect().height()/6 > player.getyPos() && y + dependencyTile.getTopTile().getPlaceRect().top - dependencyTile.getPlaceRect().height()/6 < player.getyPos()+player.getSpriteHeight() &&
                    x + dependencyTile.getPlaceRect().centerX() > player.getxPos() && x + dependencyTile.getPlaceRect().centerX() < player.getxPos() + player.getSpriteWidth());
        }
        return (y + dependencyTile.getPlaceRect().top - dependencyTile.getPlaceRect().height()/6 > player.getyPos() && y + dependencyTile.getPlaceRect().top - dependencyTile.getPlaceRect().height()/6 < player.getyPos()+player.getSpriteHeight() &&
                x + dependencyTile.getPlaceRect().centerX() > player.getxPos() && x + dependencyTile.getPlaceRect().centerX() < player.getxPos() + player.getSpriteWidth());
        /*
        if(y + dependencyTile.getPlaceRect().top - dependencyTile.getPlaceRect().height()/6 > player.getyPos() && y + dependencyTile.getPlaceRect().top - dependencyTile.getPlaceRect().height()/6 < player.getyPos()+player.getSpriteHeight() &&
                x + dependencyTile.getPlaceRect().centerX() > player.getxPos() && x + dependencyTile.getPlaceRect().centerX() < player.getxPos() + player.getSpriteWidth()) {
            return true;
        } else {
            /*if (lockedToPlayer) {
                if (y + dependencyTile.getPlaceRect().top - dependencyTile.getPlaceRect().height()/6 < player.getyPos() - player.getSpriteHeight() / 2) {
                    y += 4;
                } else if (y + dependencyTile.getPlaceRect().top - dependencyTile.getPlaceRect().height()/6 > player.getyPos() - player.getSpriteHeight() / 2) {
                    y -= 4;
                }

                if (x + dependencyTile.getPlaceRect().centerX() < player.getxPos() + player.getSpriteWidth() / 2) {
                    x += 4;
                } else if (x + dependencyTile.getPlaceRect().centerX() > player.getxPos() + player.getSpriteWidth() / 2) {
                    x -= 4;
                }
            }
            return false;
        }*/
    }

}
