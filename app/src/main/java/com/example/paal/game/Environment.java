package com.example.paal.game;

import android.graphics.Rect;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Pål on 21.02.2017.
 */

public class Environment {

    protected ArrayList<Rect> tileRects = new ArrayList<>();
    protected ArrayList<Rect> treeRects = new ArrayList<>();

    //for obstacles
    protected ArrayList<Rect> obstacleRects = new ArrayList<>();
    protected ArrayList<Rect> additionalObstacleRects = new ArrayList<>();


    protected int length_in_tiles;

    //for obstacles
    protected int obstacleSheetWidth;
    protected int obstaclesheetHeight;
    protected int obstacleWidth;
    protected int obstacleHeight;

    protected boolean hasObstacles = false;

    //for trees
    protected int treeSheetWidth;
    protected int treesheetHeight;
    protected int treeWidth;
    protected int treeHeight;


    protected int sheetWidth;
    protected int sheetHeight;
    protected int tileWidth;
    protected int tileHeight;


    public Environment(int sheetWidth, int sheetHeight, int tiles_width, int tiles_height){
        this.sheetWidth = sheetWidth;
        this.sheetHeight = sheetHeight;

        this.length_in_tiles = 100;

        tileWidth = sheetWidth/tiles_width;
        tileHeight = sheetHeight/tiles_height;
    }

    public void addObstacleSheet(int width, int height, int tiles_width, int tiles_height){
        this.obstacleSheetWidth = width;
        this.obstaclesheetHeight = height;
        this.obstacleWidth = width/tiles_width;
        this.obstacleHeight = height/tiles_height;

        hasObstacles = true;
    }

    public void makeObstacles(){
        if(hasObstacles){
            Rect stone1 = new Rect(0, 0, obstacleWidth, obstacleHeight);
            obstacleRects.add(stone1);
        }
    }

    public Obstacle generateObstacle(Tile tile){
        if(hasObstacles) {
            return new Obstacle(obstacleRects.get(0), tile);
        } else {
            return new Obstacle(new Rect(0, 0, 0, 0), tile);
        }
    }

    public Rect generateTree(){
        return treeRects.get((int)Math.floor(Math.random()*treeRects.size()));
    }

    public void addTreeSheet(int width, int height, int tiles_width, int tiles_height){
        this.treeSheetWidth = width;
        this.treesheetHeight = height;
        this.treeWidth = width/tiles_width;
        this.treeHeight = height/tiles_height;
    }

    public Tile generateTile(){
        return new Tile(tileRects);
    }

    public int getTileWidth(){
        return tileWidth;
    }

    public int getTileHeight(){
        return tileHeight;
    }

    public int getTreeHeight(){
        return treeRects.get(0).height();
    }
    public int getTreeWidth(){
        return treeRects.get(0).width();
    }
    public void addToTiles(Rect tile){
        tileRects.add(tile);
    }

    public java.util.ArrayList<Rect> getTileRects() {
        return tileRects;
    }

}
