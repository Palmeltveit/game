package com.example.paal.game;

/**
 * Created by Pål on 28.02.2017.
 */

public class RenderThread extends Thread {
    private volatile boolean running = true;
    private final static int MAX_FPS = 30;
    private final static int FRAME_PERIOD = 1000 / MAX_FPS;

    private Game game;

    public RenderThread(Game game){
        this.game = game;
    }

    @Override
    public void run() {
        long beginTime;
        long timeDiff;
        int sleepTime;

        sleepTime = 0;

        while (running) {
            beginTime = System.currentTimeMillis();

            //RENDER
            game.mDrawView.drawStuff();

            // How long time the operations took
            timeDiff = System.currentTimeMillis() - beginTime;
            // calculating sleep time
            sleepTime = (int)(FRAME_PERIOD - timeDiff);

            if (sleepTime > 0) {
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stopRunning(){
        running = false;
    }

    public void startRunning(){
        running = true;
    }

}