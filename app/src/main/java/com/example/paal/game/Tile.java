package com.example.paal.game;

import android.graphics.Rect;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Pål on 21.02.2017.
 */

public class Tile {
    private ArrayList<Rect> tileRects = new ArrayList<>();
    private Rect placeRect;

    private boolean changesSpeed = false;
    private boolean isKilltile = false;
    private boolean canHaveTrees = false;

    private int multiDx = 1;
    private int multiDy = 1;
    private int angle = 0;

    private int frames = 0;

    private boolean hasObstacle = false;
    private Obstacle obstacle;

    private boolean hasTree = false;
    private Tree tree;

    private boolean hasTopTile = false;
    private TopTile topTile;

    private boolean hasCoin = false;
    private ArrayList<Coin> coins = new ArrayList<>();

    private int currentindex = 0;

    private double offsetY = 0;

    private int width;
    private int height;

    public Tile(ArrayList<Rect> tileRects){
        this.tileRects.addAll(tileRects);

        width = tileRects.get(0).width();
        height = tileRects.get(0).height();
    }

    public void giveCoin(){
        if(Math.random() < 0.5){
            coins.add(new Coin(this, 0, 0, false));//width/2, (int)(Math.random()*height/2));
            hasCoin = true;
        } if(hasTopTile && Math.random() < 0.7){
            coins.add(new Coin(this, 0, 0, true));//imgRect.width()/2, (int)(Math.random()*imgRect.height()/2));
            hasCoin = true;
        }
    }

    public void addCoin(Coin coin){
        coins.add(coin);
    }

    public void setObstacle(Obstacle obstacle){
        this.obstacle = obstacle;
        this.hasObstacle = true;
    }

    public Obstacle getObstacle(){
        return obstacle;
    }

    public boolean hasObstacle(){
        return hasObstacle;
    }

    public void setMultiDx(int multiDx){
        this.multiDx = multiDx;
        changesSpeed = true;
    }
    public void setMultiDy(int multiDy){
        this.multiDy = multiDy;
        changesSpeed = true;
    }

    public boolean hasTopTile() {
        return hasTopTile;
    }

    public TopTile getTopTile() {
        return topTile;
    }

    public boolean isKilltile() {
        return isKilltile;
    }

    public void setKilltile(boolean isKilltile){
        this.isKilltile = isKilltile;
    }

    public void addTopTile(TopTile topTile) {
        if(offsetY == 0) {
            this.topTile = topTile;
            this.hasTopTile = true;

            if(hasObstacle && obstacle.isChest()){
                obstacle.setOnTopTile(true);
            }
        }
    }

    public boolean hasCoin() {
        return hasCoin;
    }

    public ArrayList<Coin> getCoins(){
        ArrayList<Coin> returnGetCoins = new ArrayList<>();
        returnGetCoins.addAll(coins);
        return returnGetCoins;
    }

    public void removeCoin(Coin coin){
        coins.remove(coin);
        if(coins.size() == 0) {
            hasCoin = false;
        }
    }

    public void setAngle(int angle){
        this.angle = angle;
        this.offsetY = (int)(width*Math.tan(Math.toRadians(angle)));
    }

    public void setOffsetY(int offsetY){
        if(this.angle == 0) {
            this.offsetY = offsetY;
        }
    }

    public double getOffsetY(){
        return offsetY;
    }

    public int getAngle(){
        return angle;
    }

    public void changePlayerSpeed(Player player){
        if(multiDx != 1 && !player.xSpeedChanged) {
            if(player.runningLeft){
                player.setDX(-(player.getStartingSpeed() * multiDx));
            } else {
                player.setDX(player.getStartingSpeed() * multiDx);
            }
            player.xSpeedChanged = true;
        } else if(multiDx == 1 && player.xSpeedChanged){
            if(player.runningLeft){
                player.setDX(-(player.getStartingSpeed()));
            } else {
                player.setDX(player.getStartingSpeed());
            }
            player.xSpeedChanged = false;
        }
        if (player.getDy() == 0) {
            player.setDY(-player.getStartingSpeed() * multiDy);
            player.setFloor(player.getFloor()-(int)player.getStartingSpeed() * multiDy);
        }

    }

    public void continueAnimation(){
        frames ++;
        if(frames % 8 == 0){
            if(currentindex < tileRects.size()-1){
                currentindex ++;
            } else {
                currentindex = 0;
            }
        }
    }

    public void setPlaceRect(Rect rect){
        this.placeRect = rect;
    }

    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }

    public void changePlaceRect(int x, int y){
        placeRect.left = x;
        placeRect.right = x + width;
        placeRect.top = y;
        placeRect.bottom = y + height;
    }

    public Rect getPlaceRect(){
        return placeRect;
    }

    public Rect getTileRect(){
        continueAnimation();
        return tileRects.get(currentindex);
    }

    public void setCanHaveTrees(boolean canHaveTrees){
        this.canHaveTrees = canHaveTrees;
    }

    public boolean canHaveTrees(){
        return canHaveTrees;
    }

    public boolean hasTree() {
        return hasTree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
        this.hasTree = true;
    }

    public Tree getTree() {
        return tree;
    }

    public boolean changesSpeed(){
        return changesSpeed;
    }
}
