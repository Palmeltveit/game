package com.example.paal.game;

import android.annotation.SuppressLint;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Build;
import android.os.HandlerThread;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Game extends AppCompatActivity {

    public DrawView mDrawView;

    private boolean valuesSet;

    private int thisWidth;
    private int thisHeight;

    //for registering touch values
    private double tx;
    private double ty;

    private boolean pause = false;
    private boolean pressed = false;

    private Player player;
    protected Mover mover;

    private int leapCount = 0;
    private int slideCount = 0;
    private int shootCount = 0;

    private int maxX;
    private int minX;

    private int minY;

    private double gravity = 0.66;

    private Tile currentTile;

    private GameRunnerThread gameRunner;
    private SecondaryGameRunnerThread secGameRunner;
    private GameRactingThread gameReactor;

    private RenderThread renderer;
    private RenderThread renderer2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);

        // Hide the status bar.
        View decorView = getWindow().getDecorView();

        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        //drawView (custom)
        mDrawView = (DrawView) findViewById(R.id.myDrawView);
        // Set up the user interaction to manually show or hide the system UI if single-click
        // or toggle zoom if multi-finger (two finger) click
        mDrawView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return onTouchEvent(event);
            }
        });

        setDimensions();

        mover = mDrawView.getMover();
        player = mover.getPlayer();
        player.setFloor((int)(thisHeight/1.5));

        player.setPos(thisWidth/4, (int)player.getFloor());

        mDrawView.setScreenWidthAndHeight(thisWidth, thisHeight);

        maxX = thisWidth/2;
        minY = thisHeight/2 + thisHeight/6;
        minX = 4;

        gameRunner = new GameRunnerThread(this);
        secGameRunner = new SecondaryGameRunnerThread(this);
        gameReactor = new GameRactingThread(this);
        renderer = new RenderThread(this);
        renderer2 = new RenderThread(this);

        gameRunner.start();
        secGameRunner.start();
        gameReactor.start();
        renderer.start();
        renderer2.start();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        closeBackgroundThreads();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(gameRunner == null && renderer == null) {
            openBackgroundThreads();
        }
    }

    // safely opening and closing threads
    private void openBackgroundThreads() {
        gameRunner = new GameRunnerThread(this);
        secGameRunner = new SecondaryGameRunnerThread(this);
        gameReactor = new GameRactingThread(this);
        renderer = new RenderThread(this);
        renderer2 = new RenderThread(this);

        gameRunner.start();
        secGameRunner.start();
        gameReactor.start();
        renderer.start();
        renderer2.start();
    }

    private void closeBackgroundThreads() {
        try {
            gameRunner.stopRunning();
            secGameRunner.stopRunning();
            gameReactor.stopRunning();
            renderer.stopRunning();
            renderer2.stopRunning();

            gameRunner.join();
            secGameRunner.join();
            gameReactor.join();
            renderer.join();
            renderer2.join();

            gameRunner = null;
            secGameRunner = null;
            gameReactor = null;
            renderer = null;
            renderer2 = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onTouchEvent(MotionEvent event){

        if(event.getAction() == MotionEvent.ACTION_DOWN && !pressed){
            pressed = true;
            tx = event.getX();
            ty = event.getY();
        } else if(event.getAction() == MotionEvent.ACTION_UP){
            pressed = false;
            reactToInput(event.getX(), event.getY());
        }
        return true;
    }

    private void reactToInput(double ntx, double nty){
        double yDiff = ty - nty;
        double xDiff = tx - ntx;

        if(Math.abs(yDiff) < 100 && Math.abs(xDiff) < 100){
            player.shoot();
            shootCount = 0;
        } else if(Math.abs(yDiff) > Math.abs(xDiff)) {
            if(yDiff >= 100){
                if(!player.inJump && player.getyPos() + player.getSpriteHeight()/4 >= player.getFloor()){
                    player.setDY(-15);
                    player.toggleInJump(true);
                } else {
                    player.turnOnTopTileMode();
                }
            } else if (yDiff <= -100){
                player.toggleInSlide(true);
                slideCount = 0;
                if(player.getDy() != 0){
                    player.setDY(15);
                }
            }
        } else {
            if(xDiff >= 100){
                if(player.getDx() > 0){
                    player.setDX(-player.getDx());
                    mDrawView.toggleDir(true);
                } else if(!player.inLeap){
                    player.setDX(player.getDx()*2);
                    player.toggleInLeap(true);
                    leapCount = 0;
                }
            } else if(xDiff <= -100){
                if(player.getDx() < 0) {
                    player.setDX(-player.getDx());
                    mDrawView.toggleDir(false);
                } else if(!player.inLeap){
                    player.setDX(player.getDx()*2);
                    player.toggleInLeap(true);
                    leapCount = 0;
                }
            }
        }
    }

    //changing position
    public void changePos(){

        player.move();
        if(mover.getRecentTiles().size() > 0) {
            reactToTiles();
        }
        toggleSMoves();

        //reacting to player onscreen restrictions
        if(player.getyPos() < player.getFloor() - player.getDy()){
            player.setDY(player.getDy()+gravity);
        } else {
            player.setPos(player.getxPos(), (int)player.getFloor());
            player.setDY(0);
            player.inJump = false;
        }

        if(player.getxPos() + player.getSpriteWidth() >= maxX){
            player.setPos((maxX-player.getSpriteWidth()), player.getyPos());
            mover.changeBx(player.getDx());
            player.setBx(true);

            mover.changeBgValues(mover.getBx(), mover.getBy());
        } else if(player.getxPos() <= minX){
            player.setPos(minX, player.getyPos());
        } else {
            player.setBx(false);
        }

        //score stuff
        if((player.getxPos() + mover.getBx())/25 > player.getScore()-player.getAddedToScore()){
            player.setScore((player.getxPos() + (int)mover.getBx())/25);
        }

        if(player.getScore() % 250 == 0){
            if((player.inLeap && player.getDx()/2 <= 10.6) || player.getDx() <= 10.6) {
                player.addToScore(1);
                player.changeDX(0.3);
            }
        }
    }

    public void toggleSMoves(){
        //Toggling special player moves off based on how many active frames
        if(player.inLeap){
            leapCount++;
            if(leapCount >= 20){
                player.inLeap = false;
                player.setDX(player.getDx()/2);
            }
        }
        if(player.inSlide){
            slideCount++;
            if(slideCount >= 30) {
                player.inSlide = false;
            }
        }
        if(player.inShoot){
            shootCount ++;
            if(shootCount >= 8){
                player.toggleShoot(false);
            }
        }
    }

    public void reactToTiles(){
        ArrayList<Tile> currentTiles = mover.getRecentTiles();
        int playerTileIndex =(int)Math.floor((player.getxPos()-currentTiles.get(0).getPlaceRect().left+player.getSpriteWidth()/2)/currentTiles.get(0).getWidth());
        if(currentTiles.get(playerTileIndex) != currentTile){
            currentTile = currentTiles.get(playerTileIndex);
            if (currentTile.getOffsetY() == 0) {
                player.setFloor(currentTile.getPlaceRect().top - (0.8 * player.getSpriteHeight()));
            }
        }

        //coin system
        if(currentTile.hasCoin()){
            for(Coin coin : currentTile.getCoins()) {
                coin.lockToPlayer(player);
                if (coin.moveToPlayer()) {
                    currentTile.removeCoin(coin);
                    player.addMoney();
                }
            }
        }

        if(currentTile.getAngle()!=0){
            player.setFloor(currentTile.getPlaceRect().top + currentTile.getOffsetY() - (0.8 * player.getSpriteHeight()) - (player.getxPos() + (player.getSpriteWidth()/2) - currentTile.getPlaceRect().left) * (Math.tan(Math.toRadians(currentTile.getAngle()))));
        } else {
            if(currentTile.hasObstacle()) {
                Obstacle obstacle = currentTile.getObstacle();
                if (player.getxPos() + player.getSpriteWidth()/2 >= obstacle.getPlaceRect().left
                        && player.getxPos() + player.getSpriteWidth()/2 <= obstacle.getPlaceRect().right){
                    if (player.getyPos() + player.getSpriteHeight() > obstacle.getPlaceRect().top + 0.1 * obstacle.getHeight()) {
                        if(obstacle.isChest()){
                            if(player.inLeap){
                                obstacle.open(player);
                            }
                        } else {
                            if (player.runningLeft) {
                                player.setPos(obstacle.getPlaceRect().right - player.getSpriteWidth() / 2, player.getyPos());
                            } else {
                                player.setPos(obstacle.getPlaceRect().left - player.getSpriteWidth() / 2, player.getyPos());
                            }
                        }
                        player.changeLives(obstacle.getDamageSides());
                    } else if (player.getyPos() + player.getSpriteHeight() <= obstacle.getPlaceRect().top + 0.1 * obstacle.getHeight()
                            && !(player.isOnTopTile() && player.getyPos() + player.getSpriteHeight() < obstacle.getPlaceRect().top - obstacle.getPlaceRect().height()) && !player.isOnEnemy()) {
                        player.setFloor(obstacle.getPlaceRect().top + 0.1 * obstacle.getHeight() - player.getSpriteHeight() - 1);
                        player.changeLives(obstacle.getDamageTop());
                        if(obstacle.changesPlayerSpeed() && player.getyPos() >= (obstacle.getPlaceRect().top + 0.1 * obstacle.getHeight() - player.getSpriteHeight() - 10)) {
                            player.setDY(obstacle.getDy());
                            player.toggleInJump(true);
                        }
                    }
                }
            }
            if(player.isInTopTileMode()&& !currentTile.hasTopTile() && !player.doingSomething()
                    && player.getxPos() > currentTile.getPlaceRect().right-currentTile.getPlaceRect().width()/2){
                player.turnOffTopTileMode();
            }
            if(currentTile.hasTopTile() && player.isInTopTileMode() && player.getDy() > 0){
                if(player.getyPos() + player.getSpriteHeight()/2 < currentTile.getTopTile().getPlaceRect().top) {
                    player.setFloor(currentTile.getTopTile().getPlaceRect().top - player.getSpriteHeight() + player.getSpriteHeight() / 8);
                    player.setOnTopTile(true);
                }
            }
            if((!player.isOnTopTile() || !currentTile.hasTopTile()) && player.getFloor() + mover.getDeltaBY() < minY && (!currentTile.hasObstacle()
                    || player.getxPos() + player.getSpriteWidth()/2 > currentTile.getObstacle().getPlaceRect().right)){
                player.setFloor(currentTile.getPlaceRect().top - (0.8 * player.getSpriteHeight()));
            } else if(currentTile.hasTopTile() && player.isOnTopTile()
                    && player.getFloor() <= currentTile.getTopTile().getPlaceRect().top - player.getSpriteHeight() + player.getSpriteHeight() / 8) {
                player.setFloor(currentTile.getTopTile().getPlaceRect().top - player.getSpriteHeight() + player.getSpriteHeight() / 8);
            }
            if(currentTile.getPlaceRect().top - (0.8 * player.getSpriteHeight()) + mover.getDeltaBY() < minY){
                mover.changeBy(mover.getDeltaBY() - 2);
            }
        }/*
        if(currentTile.changesSpeed() && !player.xSpeedChanged) {
            currentTile.changePlayerSpeed(player);
            player.xSpeedChanged = true;
        } else if(!currentTile.changesSpeed() && player.xSpeedChanged){
            currentTile.changePlayerSpeed(player);
            player.xSpeedChanged = false;
        }*/
    }

    private void setDimensions() {
        if (!valuesSet) {
            valuesSet = true;
            WindowManager w = this.getWindowManager();
            Display d = w.getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            d.getMetrics(metrics);

            // includes window decorations (statusbar bar/menu bar)
            if (Build.VERSION.SDK_INT >= 17) {
                try {
                    Point realSize = new Point();
                    Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
                    thisWidth = realSize.x;
                    thisHeight = realSize.y;
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        }
    }
}
