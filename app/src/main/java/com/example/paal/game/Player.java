package com.example.paal.game;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Pål on 20.02.2017.
 */

public class Player {

    private int score = 0;
    private int addedToScore = 0;

    private int money = 0;

    private int xPos;
    private int yPos;

    private final double startingSpeed = 6;
    private double dx = startingSpeed;
    private double dy = 0;
    private double floorDy = 0;

    private int spriteHeight;
    private int spriteWidth;

    private boolean bxChanging = false;

    private boolean topTileMode = false;
    private boolean onTopTile = false;

    private boolean onEnemy = false;

    private int ammo = 10;
    private ArrayList<Ammo> ammoInAir = new ArrayList<>();

    private float baseLives = 3;
    private float lives = baseLives;
    private int livesCooldown = 0;

    private double floor;

    public boolean runningLeft = false;

    public boolean inLeap = false;
    public boolean inJump = false;
    public boolean inSlide = false;
    protected boolean inShoot = false;
    public boolean xSpeedChanged = false;

    public Player(){
        this(0, 0);
    }

    public Player(int x, int y){
        this.xPos = x;
        this.yPos = y;
    }


    public void addMoney(){
        addMoney(1);
    }
    public void addMoney(int amount){
        money += amount;
    }

    public int getMoney() {
        return money;
    }

    public float getBaseLives() {
        return baseLives;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score + addedToScore;
    }

    public int getAddedToScore() {
        return addedToScore;
    }

    public void addToScore(int amount){
        score += amount;
        addedToScore += amount;
    }

    public boolean isInTopTileMode() {
        return topTileMode;
    }

    public void turnOffTopTileMode(){
        topTileMode = false;
        onTopTile = false;

    }

    public void turnOnTopTileMode(){
        topTileMode = true;
    }

    public void setFloor(double floor){
        this.floor = floor;
    }

    public boolean doingSomething(){
        return (inJump||inLeap||inSlide|inShoot);
    }

    public double getLives(){
        return lives;
    }

    public int getLivesCooldown(){
        return livesCooldown;
    }

    public boolean isOnTopTile() {
        return onTopTile;
    }

    public boolean isOnEnemy() {
        return onEnemy;
    }

    public void setOnEnemy(boolean onEnemy) {
        this.onEnemy = onEnemy;
    }

    public void setOnTopTile(boolean onTopTile) {
        this.onTopTile = onTopTile;
    }

    public void changeLives(double amount){
        if(amount < 0 && livesCooldown == 0){
            livesCooldown = 50;
            lives += amount;
        } else if (amount > 0){
            lives += amount;
        }
    }

    public void setDY(double newDY){
        dy = newDY;
    }

    public void setDX(double newDX){
        dx = newDX;
    }

    public void changeDX(double amount){
        dx += amount;
    }

    public void resetDX(){
        this.dx = startingSpeed;
    }

    public double getStartingSpeed(){
        return startingSpeed;
    }

    public double getFloor(){
        return floor;
    }

    public void move(){
        if(livesCooldown > 0) {
            livesCooldown--;
        }
        this.xPos += dx;
        if(this.yPos + dy <= floor){
            this.yPos += dy;
        } else {
            this.yPos = (int)floor;
        }
        this.floor += floorDy;

        moveAmmo();
    }

    public void setFloorDy(double newFloorDy){
        this.floorDy = newFloorDy;
    }

    public double getFloorDy(){
        return this.floorDy;
    }

    public void setPos(int x, int y){
        this.xPos = x;
        this.yPos = y;
    }

    public double getDy(){
        return dy;
    }

    public double getDx(){
        return dx;
    }

    public int getxPos(){
        return xPos;
    }

    public int getyPos(){
        return yPos;
    }

    public void toggleInJump(boolean inJump){
        this.inJump = inJump;
        inSlide = false;
        if(inLeap) {
            dx /= 2;
            inLeap = false;
        }
    }

    public void toggleInSlide(boolean inSlide){
        if(this.inSlide && inSlide){
            turnOffTopTileMode();
        }
        this.inSlide = inSlide;
        inJump = false;
        if(inLeap) {
            dx /= 2;
            inLeap = false;
        }
    }

    public void toggleInLeap(boolean inLeap){
        this.inLeap = inLeap;
        inJump = false;
        inSlide = false;
    }

    public void setBx(boolean bx){
        this.bxChanging = bx;
    }

    //setting player (sprite) width and height from decoded bitmap
    public void setSpriteWH(int width, int height){
        this.spriteHeight = height;
        this.spriteWidth = width;
    }

    public int getSpriteWidth(){
        return spriteWidth;
    }

    public int getSpriteHeight(){
        return spriteHeight;
    }
    public void increaseAmmo(int increment){
        ammo += increment;
    }

    public void shoot(){
        if(ammo > 0) {
            ammo--;
            inShoot = true;
            inSlide = false;
            inJump = false;
            if (inLeap) {
                dx /= 2;
                inLeap = false;
            }
            addAmmo(new Ammo(xPos, yPos, dx*2));
        }
    }

    public void toggleShoot(boolean shooting){
        inShoot = shooting;
    }

    public void addAmmo(Ammo ammo){
        ammoInAir.add(ammo);
    }

    public void moveAmmo(){
        for(Ammo ammo : ammoInAir){
            ammo.move(bxChanging);
        }
        while (ammoInAir.size() > 0 && (ammoInAir.get(0).getX() >= xPos + 1000 || ammoInAir.get(0).getY() > yPos + spriteHeight)) {
            ammoInAir.remove(0);
        }
    }

    public synchronized ArrayList<Ammo> getAmmoInAir(){
        ArrayList<Ammo> ammoInAirCopy = new ArrayList<>();
        ammoInAirCopy.addAll(ammoInAir);
        return ammoInAirCopy;
    }
}
