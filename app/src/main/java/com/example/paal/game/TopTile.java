package com.example.paal.game;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.util.Log;

/**
 * Created by Pål on 03.03.2017.
 */

public class TopTile {
    private Rect imgRect;
    private Tile bottomTile;


    public TopTile(Rect imgRect, Tile bottomTile){
        this.bottomTile = bottomTile;
        this.imgRect = imgRect;
    }

    public Rect getImgRect() {
        return imgRect;
    }

    public Rect getPlaceRect(){
        Rect bottomTileRect = bottomTile.getPlaceRect();
        return new Rect(bottomTileRect.left, (int)(bottomTileRect.top - (0.9*imgRect.height())), bottomTileRect.left + imgRect.width(), (int)(bottomTileRect.top+imgRect.top + (0.1*imgRect.height())));
    }
}
