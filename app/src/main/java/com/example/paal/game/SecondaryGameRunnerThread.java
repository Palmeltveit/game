package com.example.paal.game;

/**
 * Created by Pål on 27.02.2017.
 */

public class SecondaryGameRunnerThread extends Thread {

    private volatile boolean running = true;
    private final static int MAX_FPS = 50;
    private final static int FRAME_PERIOD = 1000 / MAX_FPS;

    private Game game;

    public SecondaryGameRunnerThread(Game game){
        this.game = game;
    }



    @Override
    public void run() {

        long beginTime;
        long timeDiff;
        int sleepTime = 0;

        while (running){
            beginTime = System.currentTimeMillis();

            //do work
            game.mover.moveRestStuff();

            // How long time the operations took
            timeDiff = System.currentTimeMillis() - beginTime;
            // calculating sleep time
            sleepTime = (int)(FRAME_PERIOD - timeDiff);

            if (sleepTime > 0) {
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stopRunning(){
        running = false;
    }

    public void startRunning(){
        running = true;
    }
}
