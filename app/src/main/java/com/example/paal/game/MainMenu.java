package com.example.paal.game;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by Pål on 03.03.2017.
 */

public class MainMenu extends AppCompatActivity {

    private View mPlayButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_menu);

        // Hide the status bar.
        View decorView = getWindow().getDecorView();

        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        mPlayButton = findViewById(R.id.mPlayBtn);

        mPlayButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            mPlayButton.getBackground().setColorFilter(Color.argb(55, 0, 0, 0), PorterDuff.Mode.SRC_ATOP); // Black Tint
                            return true;
                        case MotionEvent.ACTION_UP:
                            mPlayButton.getBackground().clearColorFilter(); // remove tint
                            startGame(v);
                            return true;
                    }
                return false;
            }
        });
    }

    public void startGame(View view){
        Intent i = new Intent(this, Game.class);
        startActivity(i);
    }
}
