package com.example.paal.game;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Pål on 31.12.2016.
 */

public class DrawView extends SurfaceView {

    private SurfaceHolder mSurfaceHolder;
    private Paint painter;

    private Bitmap bg_front;
    private Bitmap bg_back;

    private Bitmap playerSprite;
    private Bitmap enemySprite1;
    private Bitmap obstacles;
    private Bitmap trees;
    private Bitmap topTiles;
    private Bitmap mountains;
    private Bitmap ammo;
    private Bitmap lives;
    private Bitmap coins;

    private Mover mover;
    private Player player;

    private int screenWidth;
    private int screenHeight;

    private int ammoWidth;
    private int ammoHeight;

    private Rect coinRect;


    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);

        painter = new Paint();
        painter.setARGB(255, 34, 32, 52);
        painter.setTextSize(24 * getResources().getDisplayMetrics().density);
        painter.setFakeBoldText(true);

        mSurfaceHolder = getHolder();

        bg_front = BitmapFactory.decodeResource(getResources(), R.drawable.tileset_new);
        mountains = BitmapFactory.decodeResource(getResources(), R.drawable.bg_mountains    );
        playerSprite = BitmapFactory.decodeResource(getResources(), R.drawable.spritemap);
        enemySprite1 = BitmapFactory.decodeResource(getResources(), R.drawable.beetle5);
        obstacles = BitmapFactory.decodeResource(getResources(), R.drawable.obstacles);
        trees = BitmapFactory.decodeResource(getResources(), R.drawable.treeset);
        topTiles = BitmapFactory.decodeResource(getResources(), R.drawable.top_tiles);
        ammo = BitmapFactory.decodeResource(getResources(), R.drawable.ammo);
        lives = BitmapFactory.decodeResource(getResources(), R.drawable.lives);
        coins = BitmapFactory.decodeResource(getResources(), R.drawable.coins);

        ammoWidth = ammo.getWidth()/3;
        ammoHeight = ammo.getHeight()/4;

        this.mover = new Mover(bg_front.getWidth(), bg_front.getHeight(),
                playerSprite.getWidth(), playerSprite.getHeight(),
                enemySprite1.getWidth(), enemySprite1.getHeight(),
                obstacles.getWidth(), obstacles.getHeight(),
                trees.getWidth(), trees.getHeight(),
                topTiles.getWidth(), topTiles.getHeight(),
                mountains.getWidth(), mountains.getHeight());
        this.player = mover.getPlayer();
    }

    public void drawStuff(){
        if(mSurfaceHolder.getSurface().isValid()){
            Canvas canvas = mSurfaceHolder.lockCanvas();

            //getting all necessary arrayLists from synchronized methods in the other thread
            ArrayList<Mountain> currentMountains = mover.getMountains();
            ArrayList<Tile> currentTiles = mover.getRecentTiles();
            ArrayList<Ammo> currentAmmoInAir = player.getAmmoInAir();
            ArrayList<Enemy> currentEnemies = mover.getEnemies();


            //drawing background
            canvas.drawBitmap(bg_back, new Rect(0, 0, bg_back.getWidth(), bg_back.getHeight()), new RectF(0, 0, screenWidth, screenHeight), null);

            int mountainSize = currentMountains.size();
            for(int i = 0; i < mountainSize; i++){
                canvas.drawBitmap(mountains, currentMountains.get(i).getImgRect(), currentMountains.get(i).getPlaceRect(), null);
            }

            int coinFrame = mover.getCoinFrame();
            coinRect = new Rect(coinFrame*coins.getWidth()/8, 0, (coinFrame+1)*coins.getWidth()/8, coins.getHeight());

            int tileSize = currentTiles.size();
            for(int i = 0; i < tileSize; i++){
                Tile tile =  currentTiles.get(i);
                canvas.drawBitmap(bg_front, tile.getTileRect(), tile.getPlaceRect(), null);

                if(tile.hasTree()){
                    canvas.drawBitmap(trees, tile.getTree().getImgRect(), tile.getTree().getPlaceRect(), null);
                }
                if(tile.hasTopTile()){
                    TopTile topTile = tile.getTopTile();
                    canvas.drawBitmap(topTiles, topTile.getImgRect(), topTile.getPlaceRect(), null);
                }
                if(tile.hasObstacle()){
                    Obstacle obstacle = tile.getObstacle();
                    canvas.drawBitmap(obstacles, obstacle.getImgRect(), obstacle.getPlaceRect(), null);
                }
                if(tile.hasCoin()){
                    ArrayList<Coin> coins = tile.getCoins();
                    for(Coin coin : coins) {
                        if(coin.isOnTopTile()){
                            TopTile topTile = tile.getTopTile();
                            canvas.drawBitmap(this.coins, coinRect, new Rect(topTile.getPlaceRect().centerX() + coin.getX(), topTile.getPlaceRect().top - tile.getPlaceRect().height() / 6 - coin.getY(), topTile.getPlaceRect().centerX() + coin.getX() + coinRect.width(), topTile.getPlaceRect().top - coin.getY()), null);
                        } else {
                            canvas.drawBitmap(this.coins, coinRect, new Rect(tile.getPlaceRect().centerX() + coin.getX(), tile.getPlaceRect().top - tile.getPlaceRect().height() / 6 - coin.getY(), tile.getPlaceRect().centerX() + coin.getX() + coinRect.width(), tile.getPlaceRect().top - coin.getY()), null);
                        }
                    }
                }
                if(tile.getPlaceRect().bottom - tile.getPlaceRect().height()/6 < screenHeight){
                    canvas.drawRect(new Rect(tile.getPlaceRect().left, tile.getPlaceRect().bottom - tile.getPlaceRect().height()/6, tile.getPlaceRect().right, screenHeight), painter);
                }
            }

            //drawing sprites
            int spriteWidth = player.getSpriteWidth();
            int spriteHeight = player.getSpriteHeight();

            if(Math.floor(player.getLivesCooldown()/6) % 2 == 0) {
                canvas.drawBitmap(playerSprite, new Rect(mover.getSpriteX() * spriteWidth, mover.getSpriteY() * spriteHeight, mover.getSpriteX() * spriteWidth + spriteWidth, mover.getSpriteY() * spriteHeight + spriteHeight),
                        new RectF((float) player.getxPos(), (float) player.getyPos(), spriteWidth + (float) player.getxPos(), spriteHeight + (float) player.getyPos()), null);
            }

            int ammoSize = currentAmmoInAir.size();
            for(int i = 0; i < ammoSize; i++){
                canvas.drawBitmap(this.ammo, new Rect(0, 0, ammoWidth, ammoHeight), new RectF((float)currentAmmoInAir.get(i).getX(), (float)currentAmmoInAir.get(i).getY(), ammoWidth+(float)currentAmmoInAir.get(i).getX(), ammoHeight+(float)currentAmmoInAir.get(i).getY()), null);
            }

            int enemySize = currentEnemies.size();
            for(int i = 0; i < enemySize; i++){
                canvas.drawBitmap(this.enemySprite1, currentEnemies.get(i).getImgRect(), currentEnemies.get(i).getPlaceRect(), null);
            }

            //lives
            for(int i = 0; i < Math.floor(player.getBaseLives()); i++){
                if(i <= player.getLives()-1) {
                    canvas.drawBitmap(lives, new Rect(0, 0, lives.getWidth()/5, lives.getHeight()), new Rect(9+lives.getWidth()/5 * i, 10, lives.getWidth()/5 *(i+1), lives.getHeight()), null);
                } else if(i - 0.5 <= player.getLives()-1){
                    canvas.drawBitmap(lives, new Rect(lives.getWidth()/5, 0, 2*lives.getWidth()/5, lives.getHeight()), new Rect(9+lives.getWidth()/5 * i, 10, lives.getWidth()/5 * (i+1), lives.getHeight()), null);
                } else {
                    canvas.drawBitmap(lives, new Rect(2*lives.getWidth()/5, 0, 3*lives.getWidth()/5, lives.getHeight()), new Rect(9+lives.getWidth()/5 * i, 10, lives.getWidth()/5 * (i+1), lives.getHeight()), null);
                }
            }
            if(player.isInTopTileMode()){
                canvas.drawBitmap(lives, new Rect(3*lives.getWidth()/5, 0, 4*lives.getWidth()/5, lives.getHeight()), new Rect(lives.getWidth()/5 * 5, 10, lives.getWidth()/5 * 6, lives.getHeight()), null);
            } else {
                canvas.drawBitmap(lives, new Rect(4*lives.getWidth()/5, 0, 5*lives.getWidth()/5, lives.getHeight()), new Rect(lives.getWidth()/5 * 5, 10, lives.getWidth()/5 * 6, lives.getHeight()), null);
            }

            canvas.drawText(player.getMoney() + "$. Score: " + player.getScore(), (screenWidth/2), (int)(screenHeight*0.1), painter);

            mSurfaceHolder.unlockCanvasAndPost(canvas);

        }
    }

    public Mover getMover(){
        return mover;
    }

    public void toggleDir(boolean left){
        player.runningLeft = left;
        player.xSpeedChanged = false;
        if(left){
            playerSprite = BitmapFactory.decodeResource(getResources(), R.drawable.spritemap_left);
        } else {
            playerSprite = BitmapFactory.decodeResource(getResources(), R.drawable.spritemap);
        }
    }

    public void setScreenWidthAndHeight(int width, int height){
        this.screenWidth = width;
        this.screenHeight = height;

        mover.setScreenWidthAndHeight(width, height);
        bg_back = decodeSampledBitmapFromResource(getResources(), R.drawable.sky, screenWidth/4, screenHeight/4);
    }

    //
    //Static operations for efficient bitmap loading
    //


    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public Bitmap getBitmapOfSize(Context context, int id, int width, int height){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        return BitmapFactory.decodeResource(context.getResources(), id, options);
    }
}

