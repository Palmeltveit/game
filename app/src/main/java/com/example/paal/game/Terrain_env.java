package com.example.paal.game;

import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;


public class Terrain_env extends Environment {

    private Rect grass;
    private Rect dirt;
    private Rect water1;
    private Rect water2;
    private Rect water3;

    private Rect grassDirtLeft;
    private Rect grassDirtRight;

    private Rect leftSide;
    private Rect rightSide;

    private Rect slope;

    //for subEnvironments
    private ArrayList<SubEnvironment> subEnvironments;

    private int currentSubEnv = 0;
    private int nextSubEnv = 1;

    public Terrain_env(int sheetWidth, int sheetHeight, int tiles_width, int tiles_height){
        super(sheetWidth, sheetHeight, tiles_width, tiles_height);
        tileRects.add(grass);

        grass = new Rect(3*tileWidth, 2*tileHeight, 4*tileWidth, 3*tileHeight);
        dirt = new Rect(0, 2*tileHeight, tileWidth, 3*tileHeight);

        water1 = new Rect(5*tileWidth, 0, 6*tileWidth, tileHeight);
        water2 = new Rect(5*tileWidth, 3*tileHeight, 6*tileWidth, 4*tileHeight);
        water3 = new Rect(4*tileWidth, 3*tileHeight, 5*tileWidth, 4*tileHeight);

        grassDirtLeft = new Rect(tileWidth, 2*tileHeight, 2*tileWidth, 3*tileHeight);
        grassDirtRight = new Rect(2*tileWidth, 2*tileHeight, 3*tileWidth, 3*tileHeight);

        leftSide = new Rect(4*tileWidth, 2*tileHeight, 5*tileWidth, 3*tileHeight);
        rightSide = new Rect(5*tileWidth, 2*tileHeight, 6*tileWidth, 3*tileHeight);

        slope = new Rect(0, 3*tileHeight, tileWidth, 4*tileHeight);

        makeSubEnvironments();

    }

    private void makeSubEnvironments(){
        subEnvironments = new ArrayList<>();

        SubEnvironment grassEnv = new SubEnvironment("grass", 3, 10);
        grassEnv.setObstacles(obstacleRects, additionalObstacleRects);

        grassEnv.addNormalTile(grass, 0, false);
        grassEnv.addStartTile(grassDirtRight, 0, false);

        SubEnvironment dirtEnv = new SubEnvironment("dirt", 3, 10);
        dirtEnv.setObstacles(obstacleRects, additionalObstacleRects);

        dirtEnv.addNormalTile(dirt, 0, false);
        dirtEnv.addStartTile(grassDirtLeft, 0, false);

        SubEnvironment jumpEnv = new SubEnvironment("jump", 3, 3);

        jumpEnv.addNormalTile(water1, 0, true);
        jumpEnv.addAdditionalNormalTile(water2);
        jumpEnv.addAdditionalNormalTile(water3);
        jumpEnv.setMiddleCanHaveTrees(false);

        jumpEnv.setMiddleOffsetY((int)(-0.15 * tileHeight));

        jumpEnv.addStartTile(leftSide, 0, false);
        jumpEnv.addEndTile(rightSide, 0, false);

        SubEnvironment slopeEnv = new SubEnvironment("slope", 1, 1);
        slopeEnv.addStartTile(slope, 25, false);

        jumpEnv.setCompatibleNextSubEnvironment(dirtEnv.getName());
        jumpEnv.setCompatibleNextSubEnvironment(jumpEnv.getName());

        //grassEnv.setCompatibleNextSubEnvironment(dirtEnv.getName());
        grassEnv.setCompatibleNextSubEnvironment(slopeEnv.getName());
        grassEnv.setCompatibleNextSubEnvironment(jumpEnv.getName());

        dirtEnv.setCompatibleNextSubEnvironment(grassEnv.getName());
        dirtEnv.setCompatibleNextSubEnvironment(slopeEnv.getName());

        slopeEnv.setCompatibleNextSubEnvironment(grassEnv.getName());

        subEnvironments.add(dirtEnv);
        subEnvironments.add(grassEnv);
        subEnvironments.add(jumpEnv);
        subEnvironments.add(slopeEnv);

    }

    @Override
    public void addObstacleSheet(int width, int height, int tiles_width, int tiles_height){
        super.addObstacleSheet(width, height, tiles_width, tiles_height);
        makeObstacles();
    }

    @Override
    public void addTreeSheet(int width, int height, int tiles_width, int tiles_height){
        super.addTreeSheet(width, height, tiles_width, tiles_height);
        makeTrees();
    }

    @Override
    public void makeObstacles(){
        if(hasObstacles){
            Rect stone1 = new Rect(0, 0, obstacleWidth, obstacleHeight);
            Rect stone2 = new Rect(obstacleWidth, 0, 2*obstacleWidth, obstacleHeight);
            Rect stone3 = new Rect(2*obstacleWidth, 0, 3*obstacleWidth, obstacleHeight);
            Rect stone4 = new Rect(3*obstacleWidth, 0, 4*obstacleWidth, obstacleHeight);

            Rect shroom1a = new Rect(0, obstacleHeight, obstacleWidth, 2*obstacleHeight);
            Rect shroom1b = new Rect(obstacleWidth, obstacleHeight, 2*obstacleWidth, 2*obstacleHeight);
            Rect shroom2a = new Rect(2*obstacleWidth, obstacleHeight, 3*obstacleWidth, 2*obstacleHeight);
            Rect shroom2b = new Rect(3*obstacleWidth, obstacleHeight, 4*obstacleWidth, 2*obstacleHeight);

            Rect chest1a = new Rect(0, 2*obstacleHeight, obstacleWidth, 3*obstacleHeight);
            Rect chest1b = new Rect(obstacleWidth, 2*obstacleHeight, 2*obstacleWidth, 3*obstacleHeight);

            obstacleRects.add(0, shroom1a);
            additionalObstacleRects.add(0, shroom1b);
            obstacleRects.add(1, shroom2a);
            additionalObstacleRects.add(1, shroom2b);

            obstacleRects.add(2, chest1a);
            additionalObstacleRects.add(2, chest1b);

            obstacleRects.add(stone1);
            obstacleRects.add(stone2);
            obstacleRects.add(stone3);
            obstacleRects.add(stone4);
        }
    }

    public void makeTrees(){
        //Rect tree1 = new Rect(0, 0, treeWidth, treeHeight);
        Rect tree2 = new Rect(treeWidth, 0, 2*treeWidth, treeHeight);
        Rect tree3 = new Rect(2*treeWidth, 0, 3*treeWidth, treeHeight);
        //Rect tree4 = new Rect(3*treeWidth, 0, 4*treeWidth, treeHeight);

        //treeRects.add(tree1);
        treeRects.add(tree2);
        treeRects.add(tree3);
        //treeRects.add(tree4);
    }

    @Override
    public Obstacle generateObstacle(Tile tile){
        int index = (int)Math.floor(Math.random()*obstacleRects.size());
        Obstacle returnObstacle = new Obstacle(obstacleRects.get(index), tile);
        if(index <= 1){
            returnObstacle.setChangesPlayerSpeed(0, -20);
            returnObstacle.addImgRect(additionalObstacleRects.get(index));
        } else if (index == 2){
            if(tile.hasTopTile()) {
                returnObstacle = new Obstacle(obstacleRects.get(index), tile);
                returnObstacle.addImgRect(additionalObstacleRects.get(index));
                returnObstacle.setStayActivated(true);
            } else {
                returnObstacle = new Obstacle(obstacleRects.get(index+1), tile);
            }
        }
        return returnObstacle;
    }

    @Override
    public Tile generateTile(){
        Tile newTile = subEnvironments.get(currentSubEnv).generateTile();
        if(subEnvironments.get(currentSubEnv).isFinished()){
            changeSubEnvironment();
        }
        return newTile;
    }

    private void changeSubEnvironment(){
        currentSubEnv = nextSubEnv;
        do{
            nextSubEnv = (int)Math.floor(Math.random()*subEnvironments.size());
        } while(!subEnvironments.get(currentSubEnv).getCompatibleNextSubEnvironments().contains(subEnvironments.get(nextSubEnv).getName()));
    }
}
