package com.example.paal.game;

import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Created by Pål on 24.02.2017.
 */

public class Obstacle {
    //private Rect placeRect;
    private ArrayList<Rect> imgRects = new ArrayList<>();
    private Tile dependencyTile;

    private int currentImg = 0;
    private int framesSinceInteraction = 0;

    private boolean onTopTile = false;

    private boolean hasInteracted = false;
    private boolean stayActivated = false;

    private int height;
    private int width;

    private double damageSides = -0.5;
    private double damageTop = 0;

    private boolean changesPlayerSpeed = false;
    private boolean isChest = false;

    private int money = 0;

    private int dx = 0;
    private int dy = 0;

    public Obstacle(Rect imgRect, Tile dependencyTile){
        this.imgRects.add(imgRect);
        this.dependencyTile = dependencyTile;

        this.height = imgRect.height();
        this.width = imgRect.width();
    }

    public void setStayActivated(boolean stayActivated) {
        this.stayActivated = stayActivated;
        this.isChest = stayActivated;
        this.money = (int)(Math.floor(Math.random()*30));
        this.damageSides = 0;
    }

    public void setOnTopTile(boolean onTopTile) {
        this.onTopTile = onTopTile;
    }

    public Rect getPlaceRect(){
        if(onTopTile){
            return new Rect(dependencyTile.getPlaceRect().right - width,
                    (int) (dependencyTile.getTopTile().getPlaceRect().top - 0.7 * height),
                    (dependencyTile.getPlaceRect().right),
                    (int) (dependencyTile.getTopTile().getPlaceRect().top + 0.3 * height));
        }
        return new Rect(dependencyTile.getPlaceRect().left,
                    (int) (dependencyTile.getPlaceRect().top - 0.5 * height),
                    (dependencyTile.getPlaceRect().left + width),
                    (int) (dependencyTile.getPlaceRect().top + 0.5 * height));
    }

    public void open(Player player){
        hasInteracted = true;
        player.addMoney(money);
        money = 0;
    }

    public boolean changesPlayerSpeed() {
        return changesPlayerSpeed;
    }

    public double getDamageTop() {
        return damageTop;
    }

    public double getDamageSides() {
        return damageSides;
    }

    public boolean isChest() {
        return isChest;
    }

    public int getDx() {
        hasInteracted = true;
        return dx;
    }

    public int getDy() {
        hasInteracted = true;
        return dy;
    }

    public void setChangesPlayerSpeed(int dx, int dy) {
        this.changesPlayerSpeed = true;
        this.damageSides = 0;

        this.dy = dy;
        this.dx = dx;
    }

    public void setDamageTop(double damageTop){
        this.damageTop = damageTop;
    }

    public void addImgRect(Rect newRect){
        this.imgRects.add(newRect);
    }

    public int getHeight(){
        return this.height;
    }

    public int getWidth(){
        return this.width;
    }


    public Rect getImgRect(){
        if(hasInteracted && imgRects.size() > 1){
            framesSinceInteraction ++;
            if(framesSinceInteraction % 8 == 0 && !stayActivated){
                hasInteracted = false;
            }
            return imgRects.get(1);
        }
        return imgRects.get(0);
    }
}
