package com.example.paal.game;

import android.graphics.Rect;

/**
 * Created by Pål on 01.03.2017.
 */

public class Tree {
    private Rect imgRect;

    private int width;
    private int height;

    private Tile dependencyTile;

    private int x;
    private int y;

    public Tree(Rect imgRect, Tile dependencyTile, int offsetX){
        this.imgRect = imgRect;

        this.width = imgRect.width();
        this.height = imgRect.height();

        this.dependencyTile = dependencyTile;

        this.x = offsetX-(width/2);
    }

    public void move(int dx){
        x += dx;
    }

    public void setPos(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Rect getImgRect() {
        return imgRect;
    }

    public Rect getPlaceRect() {
        Rect dependencyTileRect = dependencyTile.getPlaceRect();
        return new Rect(dependencyTileRect.left + x, (int)(dependencyTileRect.top - (0.9*height)), dependencyTileRect.left+x+width, (int)(dependencyTileRect.top + (0.1*height)));
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
