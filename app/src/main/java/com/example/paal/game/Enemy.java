package com.example.paal.game;

import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Created by Pål on 25.02.2017.
 */

public class Enemy {

    protected int lives = 1;
    protected int damage = 1;
    private int points = 20;

    private int height;
    private int width;

    private int height_sprites = 2;
    private int width_sprites;

    private int floor;

    private int x;
    private int y;

    private int dx = -4;
    private int dy = 0;

    private int currentSprite= 0;
    private int currentDir = 1;

    public Enemy(int x, int y, int sheetWidth, int sheetHeight, int width_sprites){
        this.width_sprites = width_sprites;

        this.width = sheetWidth / width_sprites;
        this.height = sheetHeight/height_sprites;

        this.x = x;
        this.y = y;

        this.floor = y + height;
    }

    public int getxPos(){
        return x;
    }

    public int getyPos(){
        return y;
    }
    public void changePos(int x, int y){
        this.x = x;
        this.y = y;

        this.floor = y;
    }

    public int getPoints(){
        int pointsToReturn = points;
        points = 0;
        return pointsToReturn;
    }

    public void move(){
        if(lives > 0) {
            x += dx;
            if (y + height > floor || y > 0) {
                y += dy;
            }
        }
    }

    public void takeDamage(int damage){
        lives -= damage;
    }

    public int getLives(){
        return lives;
    }

    public int getDamage(){
        return damage;
    }

    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }

    public int getDx(){
        return dx;
    }

    public boolean collidingWithPlayer(Player player){
        return(player.getyPos() + player.getSpriteHeight() >= y
                && player.getxPos() + player.getSpriteWidth()/2 > x
                && player.getxPos() < x + width);
    }

    public Rect getImgRect(){
        if(lives <= 0){
            return new Rect(5 * width, currentDir * height, 6 * width, (currentDir + 1) * height);
        } else {
            return new Rect(currentSprite * width, currentDir * height, (currentSprite + 1) * width, (currentDir + 1) * height);
        }
    }

    public Rect getPlaceRect(){
        return new Rect(x, y, x+width, y+height);
    }

    public void setGoingLeft(boolean left){
        if(left){
            currentDir = 1;
            dx = -Math.abs(dx);
        } else {
            currentDir = 0;
            dx = Math.abs(dx);
        }
    }

    public void changeSpriteImg(int frameCount){
        if(frameCount % 4 == 0){
            if(currentSprite < width_sprites-2){
                currentSprite ++;
            } else {
                currentSprite = 0;
            }
        }
    }

}
