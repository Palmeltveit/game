package com.example.paal.game;

import android.graphics.Rect;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Pål on 22.02.2017.
 */
public class SubEnvironment {

    private ArrayList<Rect> startTiles = new ArrayList<>();
    private ArrayList<Rect> endTiles = new ArrayList<>();
    private ArrayList<Rect> normalTiles = new ArrayList<>();

    private ArrayList<Rect> obstacleRects = new ArrayList<>();
    private ArrayList<Rect> additionalObstacleRects = new ArrayList<>();

    private int minLength = 3;
    private int maxLength;

    private int length;
    private int tileNum = 0;

    private String name;
    private ArrayList<String> compatibleNextSubEnvironments;
    private HashMap<Rect, Integer> angles = new HashMap<>();
    private HashMap<Rect, Boolean> killTiles = new HashMap<>();

    private boolean hasEnd = false;
    private boolean hasObstacles = false;
    private boolean middleCanHaveTrees = true;

    private int middleOffsetY = 0;

    public SubEnvironment(String name, int minLength){
        this(name, minLength, 20);
    }

    public SubEnvironment(String name, int minLength, int maxLength){

        this.name = name;

        compatibleNextSubEnvironments = new ArrayList<>();

        this.minLength = minLength;
        this.maxLength = Math.abs(maxLength);
        this.length = (int)Math.floor(Math.random()*(maxLength-minLength))+minLength;

    }

    public void addStartTile(Rect tile, int angle, boolean kills){
        startTiles.add(tile);
        angles.put(tile, angle);
        killTiles.put(tile, kills);
    }

    public void addNormalTile(Rect tile, int angle, boolean kills){
        normalTiles.add(tile);
        angles.put(tile, angle);
        killTiles.put(tile, kills);
    }

    public void addAdditionalStartTile(Rect tile){
        normalTiles.add(tile);
    }
    public void addAdditionalNormalTile(Rect tile){
        normalTiles.add(tile);
    }
    public void addAdditionalEndTile(Rect tile){
        normalTiles.add(tile);
    }

    public void addEndTile(Rect tile, int angle, boolean kills){
        endTiles.add(tile);
        angles.put(tile, angle);
        killTiles.put(tile, kills);
        hasEnd = true;
    }

    public boolean isFinished(){
        if((tileNum >= length)){
            tileNum = 0;
            length = (int)Math.floor(Math.random()*(maxLength-minLength))+minLength;
            return true;
        }
        return false;
    }

    public void setMiddleOffsetY(int offsetY){
        this.middleOffsetY = offsetY;
    }

    public boolean isHasObstacles() {
        return hasObstacles;
    }

    public void setObstacles(ArrayList<Rect> obstacles, ArrayList<Rect> additionals){
        this.obstacleRects = obstacles;
        this.additionalObstacleRects = additionals;

        this.hasObstacles = true;
    }

    public Tile generateTile(){
        tileNum ++;
        if(tileNum == 1){
            Tile returnStartTile =  new Tile(startTiles);
            returnStartTile.setKilltile(killTiles.get(startTiles.get(0)));
            if(angles.get(startTiles.get(0)) != 0) {
                returnStartTile.setAngle(angles.get(startTiles.get(0)));
            }
            return returnStartTile;
        } else if(hasEnd && tileNum == length) {
            Tile returnEndTile = new Tile(endTiles);
            returnEndTile.setKilltile(killTiles.get(endTiles.get(0)));
            if(angles.get(endTiles.get(0)) != 0) {
                returnEndTile.setAngle(angles.get(endTiles.get(0)));
            }
            return returnEndTile;
        } else {
            Tile returnNormalTile = new Tile(normalTiles);
            if(hasObstacles && Math.random() < 0.2){
                int index = (int)Math.floor(Math.random()*obstacleRects.size());
                Obstacle obstacle = new Obstacle(obstacleRects.get(index), returnNormalTile);
                if(index <= 1){
                    obstacle.setChangesPlayerSpeed(0, -20);
                    obstacle.addImgRect(additionalObstacleRects.get(index));
                } else if (index == 2) {
                    obstacle = new Obstacle(obstacleRects.get(index), returnNormalTile);
                    obstacle.addImgRect(additionalObstacleRects.get(index));
                    obstacle.setStayActivated(true);
                }
                returnNormalTile.setObstacle(obstacle);
            }
            returnNormalTile.setCanHaveTrees(middleCanHaveTrees);
            returnNormalTile.setKilltile(killTiles.get(normalTiles.get(0)));
            returnNormalTile.setOffsetY(middleOffsetY);
            if(angles.get(normalTiles.get(0)) != 0) {
                returnNormalTile.setAngle(angles.get(normalTiles.get(0)));
            }
            return returnNormalTile;
        }
    }

    public void setMiddleCanHaveTrees(boolean canHaveTrees){
        this.middleCanHaveTrees = canHaveTrees;
    }

    public void setCompatibleNextSubEnvironment(String name){
        compatibleNextSubEnvironments.add(name);
    }

    public ArrayList<String> getCompatibleNextSubEnvironments(){
        return compatibleNextSubEnvironments;
    }

    public String getName(){
        return name;
    }
}
