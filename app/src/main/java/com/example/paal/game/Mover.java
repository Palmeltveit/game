package com.example.paal.game;

import android.graphics.Rect;
import android.nfc.Tag;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pål on 27.02.2017.
 */

public class Mover {

    private Player player;

    //for generating tiles
    public Environment environment;

    private int tileX = 0;
    private double deltaBX = 0;
    private double deltaBY = 0;

    private double secondarydeltaBX = 0;
    private double secondarydeltaBY = 0;

    private ArrayList<Tile> recentTiles = new ArrayList<>();

    private ArrayList<Enemy> enemies = new ArrayList<>();

    private ArrayList<Rect> mountainRects = new ArrayList<>();
    private ArrayList<Mountain> recentMountains = new ArrayList<>();

    private ArrayList<Rect> topTileRects = new ArrayList<>();

    private int mountainWidth;
    private int mountainHeight;

    private double bx = 0;
    private double by = 0;

    private int screenWidth;
    private int screenHeight;

    private int sprites_horizontal = 8;
    private int sprites_vertival = 4;

    private int eSpriteSheetWidth;
    private int eSpriteSheetHeight;


    //player spriteSheet values
    private int spriteX = 0;
    private int spriteY = 3;

    // OTHER PART OF LOOP

    private int leapCount = 0;
    private int slideCount = 0;
    private int shootCount = 0;

    private int maxX;
    private int minX;
    private int minY;

    private double gravity = 0.66;

    private Tile currentTile;

    private double floor = -1;

    public int numberOfFrames = 0;
    private int coinFrame = 0;

    public Mover(int width, int height,
                 int pSpriteSheetWidth, int pSpriteSheetHeight,
                 int eSpriteSheetWidth, int eSpriteSheetHeight,
                 int obstacleSheetWidth, int obstacleSheetHeight,
                 int treeSheetWidth, int treeSheetHeight,
                 int topTileSheetWidth, int topTileSheetHeight,
                 int mountainSheetWidth, int mountainSheetHeight){
        player = new Player();
        player.setSpriteWH(pSpriteSheetWidth/sprites_horizontal, pSpriteSheetHeight/sprites_vertival);

        this.eSpriteSheetHeight = eSpriteSheetHeight;
        this.eSpriteSheetWidth = eSpriteSheetWidth;

        environment = new Terrain_env(width, height, 6, 4);
        environment.addObstacleSheet(obstacleSheetWidth, obstacleSheetHeight, 4, 3);
        environment.addTreeSheet(treeSheetWidth, treeSheetHeight, 6, 1);

        makeTopTiles(topTileSheetWidth, topTileSheetHeight);
        makeMountains(mountainSheetWidth, mountainSheetHeight);
    }

    private void makeTopTiles(int topTileSheetWidth, int topTileSheetHeight){
        topTileRects.add(new Rect(0, 0, topTileSheetWidth, topTileSheetHeight));
    }

    private void makeMountains(int mountainSheedWidth, int mountainSheetHeight){
        mountainWidth = mountainSheedWidth/3;
        mountainHeight = mountainSheetHeight;
        for(int i = 0; i < 3; i++){
            mountainRects.add(new Rect(i*mountainWidth, 0, (i+1)*mountainWidth, mountainSheetHeight));
        }
    }

    public void moveStuff(){

        //adding new stuff (tiles, enemies etc.)
        addStuff();

        //moving the stuff
        moveTiles();
        //moveEnemies();

        //resetting the variables used for moving
        this.deltaBX = 0;
        this.deltaBY = 0;
    }

    public void moveRestStuff(){

        moveMountains();
        moveEnemies();

        //player animation
        continueAnimation();

        this.secondarydeltaBX = 0;
        this.secondarydeltaBY = 0;
    }

    public void addStuff(){
        if(floor < 0) {
            floor = (int)(player.getFloor() + (0.8 * player.getSpriteHeight()));
        }

        while (tileX < screenWidth + environment.tileWidth + bx) {
            Tile newTile = environment.generateTile();
            newTile.setPlaceRect(new Rect(tileX, (int)(floor - newTile.getOffsetY()), tileX + environment.getTileWidth(), (int)(floor - newTile.getOffsetY() + environment.getTileHeight())));

            if(newTile.getAngle()!=0){
                floor -= environment.getTileWidth()*Math.tan(Math.toRadians(newTile.getAngle()));
            }


            //adding stuff to tiles
            if(Math.random() < 0.5){
                newTile.addTopTile(new TopTile(topTileRects.get((int)(Math.floor(Math.random()*topTileRects.size()))), newTile));
            }
            if(recentTiles.size() > screenWidth/(newTile.getWidth()*2) || recentTiles.size() < 3) {
                if (Math.random() < 0.2 && recentTiles.size() > 6) {
                    enemies.add(new Enemy(screenWidth, (int) (newTile.getPlaceRect().top - (0.2 * player.getSpriteHeight())), eSpriteSheetWidth, eSpriteSheetHeight, 6));
                }
            }

            if(!newTile.hasTopTile() && Math.random() < 0.3  && newTile.canHaveTrees() && recentTiles.size() > 6){
                newTile.setTree(new Tree(environment.generateTree(), newTile, (int)(Math.floor(Math.random()*newTile.getWidth()))));
            }


            newTile.giveCoin();

            recentTiles.add(newTile);

            tileX += environment.getTileWidth();
        }
    }
    private void continueAnimation(){
        numberOfFrames++;

        if (numberOfFrames % 4 == 0) {
            if (player.runningLeft) {
                if(player.inSlide){
                    spriteX = 6;
                    spriteY = 2;
                } else if(player.inShoot){
                    spriteX = 5;
                    spriteY = 1;
                } else if(player.inLeap){
                    spriteX = 4;
                    spriteY = 1;
                } else if(player.inJump){
                    if(player.getDy() < 0){
                        spriteX =1;
                        spriteY = 0;
                    } else if(player.getDy() > 0){
                        spriteX = 0;
                        spriteY = 0;
                    }
                } else {
                    spriteY = 3;
                    if (spriteX > 0) {
                        spriteX--;
                    } else {
                        spriteX = 7;
                    }
                }
            } else {
                if(player.inSlide){
                    spriteX = 1;
                    spriteY = 2;
                } else if(player.inShoot){
                    spriteX = 2;
                    spriteY = 1;
                } else if(player.inLeap){
                    spriteX = 3;
                    spriteY = 1;
                } else if(player.inJump){
                    if(player.getDy() < 0){
                        spriteX = 6;
                        spriteY = 0;
                    } else if(player.getDy() > 0){
                        spriteX = 7;
                        spriteY = 0;
                    }
                } else {
                    spriteY = 3;
                    if (spriteX < 7) {
                        spriteX++;
                    } else {
                        spriteX = 0;
                    }
                }
            }
        }

        if(numberOfFrames % 6 == 0){
            if(coinFrame < 7){
                coinFrame ++;
            } else {
                coinFrame = 0;
            }
        }

    }

    public void setScreenWidthAndHeight(int width, int height){
        this.screenWidth = width;
        this.screenHeight = height;

        maxX = width/2;
        minY = height/2 + height/6;
        minX = 4;
    }

    public void changeBgValues(double bx, double by){
        if(bx > 0 && bx < screenWidth) {
            if(this.bx - bx != 0) {
                this.bx = bx;
                this.by = by;
            }
        }
    }

    public void changeBx(double increment){
        bx += increment;
        deltaBX = increment;
        secondarydeltaBX = increment;
    }

    public void changeBy(double increment){
        by += increment;
        deltaBY = increment;
        secondarydeltaBX = increment;
    }

    public double getBx(){
        return bx;
    }

    public double getDeltaBY(){
        return deltaBY;
    }

    public double getDeltaBX(){
        return deltaBX;
    }

    public double getBy(){
        return by;
    }

    public int getCoinFrame() {
        return coinFrame;
    }

    public int getSpriteX(){
        return spriteX;
    }

    public int getSpriteY(){
        return spriteY;
    }

    public Player getPlayer(){
        return player;
    }

    public synchronized ArrayList<Tile> getRecentTiles(){
        ArrayList<Tile> recentTilesCopy = new ArrayList<>();
        recentTilesCopy.addAll(recentTiles);
        return recentTilesCopy;
    }

    public synchronized ArrayList<Enemy> getEnemies(){
        ArrayList<Enemy> enemiesCopy = new ArrayList<>();
        enemiesCopy.addAll(enemies);
        return enemiesCopy;
    }


    public synchronized ArrayList<Mountain> getMountains(){
        ArrayList<Mountain> mountainsCopy = new ArrayList<>();
        mountainsCopy.addAll(recentMountains);
        return mountainsCopy;
    }


    public void moveTiles() {

        ArrayList<Tile> recentTiles = getRecentTiles();

        int x = recentTiles.get(0).getPlaceRect().left - (int) deltaBX;
        int y = recentTiles.get(0).getPlaceRect().top - (int) deltaBY;

        if(recentTiles.get(0).getOffsetY() != 0 && recentTiles.get(0).getAngle() == 0){
            y = recentTiles.get(1).getPlaceRect().top - (int) deltaBY;
        }

        recentTiles.get(0).changePlaceRect(x, recentTiles.get(0).getPlaceRect().top - (int) deltaBY);
        for (int i = 1; i < recentTiles.size(); i++) {
            recentTiles.get(i).changePlaceRect(x + (recentTiles.get(i).getWidth() * i), y - (int)recentTiles.get(i).getOffsetY());
            if(recentTiles.get(i).getAngle() > 0){
                y = recentTiles.get(i).getPlaceRect().top;
            }
        }
        while (recentTiles.get(0).getPlaceRect().right < 0) {
            recentTiles.remove(0);
        }

        this.recentTiles = recentTiles;
    }

    public void moveMountains() {

        if(recentMountains.size() == 0 || recentMountains.get(recentMountains.size()-1).getPlaceRect().right < screenWidth) {
            if(Math.random() < 0.2) {
                recentMountains.add(new Mountain(mountainRects.get((int) (Math.floor(Math.random() * 3))), screenWidth, screenHeight/2 + screenHeight/4 + player.getSpriteHeight() - mountainHeight));
            }
        }

        //double dx = secondarydeltaBX/3;
        //double dy = secondarydeltaBY/3;

        if(recentMountains.size() > 0) {
            int size = recentMountains.size();
            for (int i = 0; i < size; i++) {
                recentMountains.get(i).changePos( -secondarydeltaBX*recentMountains.get(i).getSpeedDiff(), -secondarydeltaBY*recentMountains.get(i).getSpeedDiff());
            }
            while (recentMountains.size() > 0 && recentMountains.get(0).getPlaceRect().right < 0) {
                recentMountains.remove(0);
            }
        }
    }

    public void moveEnemies(){
        ArrayList<Enemy> enemies = getEnemies();

        Tile enemyTile;
        player.setOnEnemy(false);
        int size = enemies.size();
        for(int i = 0; i < size; i++){
            Enemy enemy = enemies.get(i);
            if(enemy.getxPos() < screenWidth + enemy.getWidth()) {
                enemy.move();
            }
            enemy.changeSpriteImg(numberOfFrames);
            int enemyTileIndex = (int)Math.abs(Math.floor((enemy.getxPos()-recentTiles.get(0).getPlaceRect().left+enemy.getWidth()/2)/recentTiles.get(0).getWidth()));
            if(recentTiles.size() > enemyTileIndex) {
                enemyTile = recentTiles.get(enemyTileIndex);

                if (enemyTile.isKilltile()) {
                    if (enemy.getLives() > 0) {
                        if (enemy.getDx() > 0) {
                            enemy.changePos(enemyTile.getPlaceRect().left + (int) (Math.random() * enemy.getWidth() / 4), enemy.getyPos());
                        } else {
                            enemy.changePos(enemyTile.getPlaceRect().right - enemy.getWidth() - (int) (Math.random() * enemy.getWidth() / 4), enemy.getyPos());
                        }
                        enemy.takeDamage(10);
                    }
                }
                if (enemyTile.getAngle() == 0) {
                    enemy.changePos(enemy.getxPos() - (int) secondarydeltaBX, (int) (enemyTile.getPlaceRect().top - 0.2 * player.getSpriteHeight()));
                } else {
                    enemy.changePos(enemy.getxPos() - (int) secondarydeltaBX, (int) (enemyTile.getPlaceRect().top + enemyTile.getOffsetY() - 0.2 * player.getSpriteHeight() - (enemy.getxPos() + (enemy.getWidth() / 2) - enemyTile.getPlaceRect().left) * (Math.tan(Math.toRadians(enemyTile.getAngle())))));
                }

                if (enemyTile.hasObstacle() && !enemyTile.getObstacle().isChest()) {
                    Obstacle obstacle = enemyTile.getObstacle();
                    if ((enemy.getxPos() < obstacle.getPlaceRect().right && enemy.getxPos() > obstacle.getPlaceRect().left)
                            || ((enemy.getxPos() + enemy.getWidth() < obstacle.getPlaceRect().right
                            && enemy.getxPos() + enemy.getWidth() > obstacle.getPlaceRect().left))) {
                        if (enemy.getDx() > 0) {
                            enemy.changePos(obstacle.getPlaceRect().left - enemy.getWidth(), enemy.getyPos());
                            enemy.setGoingLeft(true);
                        } else {
                            enemy.changePos(obstacle.getPlaceRect().right, enemy.getyPos());
                            enemy.setGoingLeft(false);
                        }
                    }
                }
            }

            if(enemy.collidingWithPlayer(player)){
                if(player.getDy() > 0 || enemy.getLives() <= 0){
                    enemy.takeDamage(1);
                    player.setFloor(enemy.getPlaceRect().top+enemy.getHeight()/4 - (0.8 * player.getSpriteHeight()));
                    player.setOnEnemy(true);
                    player.addToScore(enemy.getPoints());
                } else {
                    player.changeLives(-enemy.getDamage());
                }
            } else {
                player.setOnEnemy(false);
            }
        }
        while (true) {
            if (!enemies.isEmpty() && enemies.get(0).getxPos()+enemies.get(0).getWidth() < 0) {
                enemies.remove(0);
            } else {
                break;
            }
        }

        this.enemies = enemies;
    }

    // OTHER PART OF LOOP

    //changing position
    public void changePos(){

        player.move();
        if(recentTiles.size() > 0) {
            //reactToTiles();
        }
        toggleSMoves();

        //reacting to player onscreen restrictions
        if(player.getyPos() < player.getFloor() - player.getDy()){
            player.setDY(player.getDy()+gravity);
        } else {
            player.setPos(player.getxPos(), (int)player.getFloor());
            player.setDY(0);
            player.inJump = false;
        }

        if(player.getxPos() + player.getSpriteWidth() >= maxX){
            player.setPos((maxX-player.getSpriteWidth()), player.getyPos());
            changeBx(player.getDx());
            player.setBx(true);

            changeBgValues(bx, by);
        } else if(player.getxPos() <= minX){
            player.setPos(minX, player.getyPos());
        } else {
            player.setBx(false);
        }

        /*
        if((player.getxPos() + mDrawView.getBx())/100 > score){
            score = (player.getxPos() + (int)mDrawView.getBx())/100;
            mScoreView.setText("Score: " + score + " lives: " + player.getLives());
        }*/

    }

    public void toggleSMoves(){
        //Toggling special player moves off based on how many active frames
        if(player.inLeap){
            leapCount++;
            if(leapCount >= 20){
                player.inLeap = false;
                player.setDX(player.getDx()/2);
            }
        }
        if(player.inSlide){
            slideCount++;
            if(slideCount >= 30) {
                player.inSlide = false;
            }
        }
        if(player.inShoot){
            shootCount ++;
            if(shootCount >= 8){
                player.toggleShoot(false);
            }
        }
    }
/*
    public void reactToTiles(){
        ArrayList<Tile> currentTiles = getRecentTiles();
        int playerTileIndex =(int)Math.floor((player.getxPos()-currentTiles.get(0).getPlaceRect().left+player.getSpriteWidth()/2)/currentTiles.get(0).getWidth());
        if(currentTiles.get(playerTileIndex) != currentTile){
            currentTile = currentTiles.get(playerTileIndex);
            if(!player.runningLeft && currentTile.getAngle() != 0) {
                player.setFloor(player.getFloor() - (player.getxPos() + (player.getSpriteWidth()/2) - currentTile.getPlaceRect().left) * (Math.tan(Math.toRadians(currentTile.getAngle()))));
            }
            if (currentTile.getOffsetY() == 0) {
                player.setFloor(currentTile.getPlaceRect().top - (0.8 * player.getSpriteHeight()));
            }
        }
        if(currentTile.getAngle()!=0){
            player.setFloor(currentTile.getPlaceRect().top - (0.8 * player.getSpriteHeight()) + currentTile.getOffsetY() - (player.getxPos() + (player.getSpriteWidth()/2) - currentTile.getPlaceRect().left) * (Math.tan(Math.toRadians(currentTile.getAngle()))));
        } else {
            if(currentTile.hasObstacle()) {
                Obstacle obstacle = currentTile.getObstacle();
                if (player.getxPos() + player.getSpriteWidth()/2 >= obstacle.getPlaceRect().left
                        && player.getxPos() + player.getSpriteWidth()/2 <= obstacle.getPlaceRect().right){
                    if (player.getyPos() + player.getSpriteHeight() > obstacle.getPlaceRect().top + 0.1 * obstacle.getHeight()) {
                        if (player.runningLeft) {
                            player.setPos(obstacle.getPlaceRect().right - player.getSpriteWidth()/2, player.getyPos());
                        } else {
                            player.setPos(obstacle.getPlaceRect().left - player.getSpriteWidth()/2, player.getyPos());
                        }
                        player.changeLives(obstacle.getDamage());
                    } else if (player.getyPos() + player.getSpriteHeight() <= obstacle.getPlaceRect().top + 0.1 * obstacle.getHeight()
                            && !player.isOnTopTile() && !player.isOnEnemy()) {
                        player.setFloor(obstacle.getPlaceRect().top + 0.1 * obstacle.getHeight() - player.getSpriteHeight() - 1);
                        if(obstacle.changesPlayerSpeed() && player.getyPos() >= (obstacle.getPlaceRect().top + 0.1 * obstacle.getHeight() - player.getSpriteHeight() - 10)) {
                            player.setDY(obstacle.getDy());
                            player.toggleInJump(true);
                        }
                    }
                }
            }
            if(player.isInTopTileMode()&& !currentTile.hasTopTile() && !player.doingSomething()){
                player.turnOffTopTileMode();
            }
            if(currentTile.hasTopTile() && player.isInTopTileMode()){
                if(player.getyPos() < currentTile.getTopTile().getPlaceRect().top) {
                    player.setFloor(currentTile.getTopTile().getPlaceRect().top - player.getSpriteHeight() + player.getSpriteHeight() / 6);
                    player.setOnTopTile(true);
                }
            }
            if(player.getFloor() + getDeltaBY() < minY && (!currentTile.hasObstacle()
                    || player.getxPos() + player.getSpriteWidth()/2 > currentTile.getObstacle().getPlaceRect().right)
                    && !player.isOnTopTile() && !player.isOnEnemy()){
                player.setFloor(currentTile.getPlaceRect().top - (0.8 * player.getSpriteHeight()));
            }
            if(currentTile.getPlaceRect().top - (0.8 * player.getSpriteHeight()) + getDeltaBY() < minY){
                changeBy(getDeltaBY() - 2);
            }
        }/*
        if(currentTile.changesSpeed() && !player.xSpeedChanged) {
            currentTile.changePlayerSpeed(player);
            player.xSpeedChanged = true;
        } else if(!currentTile.changesSpeed() && player.xSpeedChanged){
            currentTile.changePlayerSpeed(player);
            player.xSpeedChanged = false;
        }*/
    //}

    public void setShootCount(int newCount){
        shootCount = newCount;
    }

    public void setLeapCount(int newCount){
        leapCount = newCount;
    }

    public void setSlideCount(int newCount){
        slideCount = newCount;
    }

}
